﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IdentityApplication.Models
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<PositionRoles> PositionRoles { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer(new DataInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins");
        }
    }
}