﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace IdentityApplication.Models
{
    public class DataInitializer: DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            if (!context.Roles.Any())
            {
                context.Roles.AddOrUpdate(new Role(){Id =  1, Name = "ADMIN"});
                context.Roles.AddOrUpdate(new Role(){Id =  2, Name = "USER"});
                context.Roles.AddOrUpdate(new Role(){Id =  3, Name = "MANAGER"});
            }
            context.Users.AddOrUpdate(new User()
            {
               Id = 1, Email = "user@user.kz",EmailConfirmed = true, Roles = { new UserRole(){RoleId = 1, UserId = 0}}
            });
            context.SaveChanges();
            base.Seed(context);
        }

    }
}