﻿using System;
using System.Collections.Generic;

namespace IdentityApplication.Models
{
    public class Position
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}