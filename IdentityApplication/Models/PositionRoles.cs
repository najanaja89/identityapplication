﻿using System;
using System.Collections.Generic;

namespace IdentityApplication.Models
{
    public class PositionRoles
    {
        public int PositionId { get; set; }
        public int RoleId { get; set; }
    }
}