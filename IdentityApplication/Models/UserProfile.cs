﻿using System;
using System.Collections.Generic;

namespace IdentityApplication.Models
{
    public class UserProfile
    {
        public int UserId { get; set; }
        public int PositionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}